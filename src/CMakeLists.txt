# Include libraries
find_package(Boost REQUIRED COMPONENTS filesystem system)
find_package(Freetype REQUIRED)
find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)
find_package(PulseAudio REQUIRED)

pkg_check_modules(LAPI REQUIRED ubuntu-platform-api)

find_library(LIBGLES_1 GLESv1_CM)
if(NOT LIBGLES_1)
	message(FATAL_ERROR "Can't find GLESv1_CM")
endif()

find_library(LIBGLES_2 GLESv2)
if(NOT LIBGLES_2)
	message(FATAL_ERROR "Can't find GLESv2")
endif()

file(GLOB allFiles
	"*.h"
	"*.cpp"
)

# Add executable
add_executable(tuxracer ${allFiles})
target_include_directories(tuxracer PRIVATE ${Boost_INCLUDE_DIRS} ${FREETYPE_INCLUDE_DIRS} ${OPENGL_INCLUDE_DIRS} ${PulseAudio_INCLUDE_DIRS} ${GLUT_INCLUDE_DIRS} ${LAPI_INCLUDE_DIRS})

# Add target compile options
target_compile_options(tuxracer PRIVATE -Wall -Wextra -Wno-unused-parameter -fsingle-precision-constant)

# Add definitions
target_compile_definitions(tuxracer PRIVATE -DUSE_GLES1)

if (HAVE_DESKTOP_KIT)
	# When run in the "desktop" kit the application is not packaged and is
	# run from the CMake build directory, see e.g.:
	# https://bazaar.launchpad.net/~ubuntu-sdk-team/qtcreator-plugin-ubuntu/trunk/view/470/src/ubuntu/device/container/ubuntulocalrunconfiguration.cpp?#L140
	#
	# Also, because of https://bugs.launchpad.net/ubuntu/+source/qtcreator-plugin-ubuntu/+bug/1667119
	# the working directory is quite useless - it's the user's home directory.
	#
	# As a work-around, pass the source directory via a #define to the compilation.
	# This way it can be used in the sources to hardcode resources search path.
	target_compile_definitions(tuxracer PRIVATE
		HAVE_DESKTOP_KIT
		RESOURCES_BASE_DIRECTORY="${CMAKE_SOURCE_DIR}"
	)
endif()

target_link_libraries(tuxracer SDL2pp ${FREETYPE_LIBRARIES} ${OPENGL_LIBRARIES} ${GLUT_LIBRARIES} ${PulseAudio_INCLUDE_LIBRARIES} ${LIBGLES_1} ${LIBGLES_2} ${LAPI_LIBRARIES})

# Set Libs

install(TARGETS tuxracer RUNTIME DESTINATION .)
